# WtD, Portland, 2019

## Monday
### Any friend of the docs is a friend of mine: Cultivating a community of documentation advocates - Heather Stenson
- share customer feedback so stakeholders see that there's chatter and recognition
- docs sprint swag
    - what do stickers actually cost?
    - make it fun, rewarding, and worth participating?
- ++ stewardship and "help the helpers"
- `@docs-friend` slack handle - to get news/updates/headsup/requests?
- be the silo-bridger
- share wins/successes/feedback

### How to edit other people's content without pissing them off - Ingrid Towey
- care about your editee
    - edit in a way that makes them want to make your suggested changes
- editing as ~~suggestions~~ collaboration
    - pick your battles
    - create and use a style guide
        - ibm / more
    - use readability tests
        - readable.io / hemingway editor / ms word / acrolinx
    - follow writing standards - both for standardization and for making it easier to explain why to the writer
        - ditta(?)
    - usability testing
- "we're on the same side" / consider their point and intended audience (++empathy)
- "people read only 20% of page content"
    - make headings concise and grabbing (+better for searching/SEOing)
- be kind

### Writer? Editor? Teacher? - Kathleen Juell (Edward's notes)
- Writing docs as a method of teaching
- Keeping in mind a Teaching Philosophy
    - why do we produce documentation?
    - what assumptions do you have about the reader
    - what's the workflow you expect the reader to follow (being able to work through a doc without needing to detour)
- "Understanding by Design"
    - Create the documentation to let users experience the documentation in a way that they learn how to use the product > just copying and pasting
    - Make the product so that it doesn't need separate documentation?
- Editing as a form of teaching
    - provide a template that includes doc structure, section intros, code snippets, example headings, reusable meta, etc.
    - explain your edits / include grammar (but not as a sole reason for the edit) / as conversation
    - have a goal in mind
- Writing as Editing
    - talk to yourself - what would you want to see if you were editing this documentation?

### Writer, Editor, Teacher (Alex's notes)
- Teaching Philosophy statement - How you teach, and why you teach that way.
(Pull slides for the Venn diagram)
- Teaching in Your Intro
  - Context and Definitions
  - Frame the Why
  - Explain the How

- Achieving Understanding
 -

- Editorial Templates
  - Code
    - Explain what it will do _first_
    - Share the code
    - Explain the key concepts of the code in more detail

- Peer editing
  - Explain your thinking
  - Treat grammar like a high-level concept - bring it to your peers instead of just making the changes
  - Ask question in your peer review. Clarifies your understanding, but gives your peer a chance to sharpen their expression.

- Writing as a Teacher
  - write like a teacher
    - Clarify your why
    - Have an understanding goal (what would authentic performance look like for your doc?)
    - Gauge your audience - how many experiences can you accommodate?

- Writing as an Editor
  - Clarify your why
  - use templates
  - Adopt a beginners mind (particularly as you revise) - Explain your own thinking to yourself.

### Documenting for Open Source - Shannon Crabill (Edward's Notes)
- Make the language encouraging and inclusive of people to whom, maybe it isn't easy
- do the thing yourself as though you're the user
    - look at it as a totally new user, and get another set of eyes to avoid assumptions
- pull request template that helps discourage duplicates and to encourage including useful information and suggested changes
- consider a changelog as meta-documentation
- give contributors an easy path: readme + contributing + wiki + templates + style guide + license

### Documenting Open-Source Software (Alex's Notes)
- 93% of survey respondents cite incomplete or outdated documentation stopping them from contributing to open-source projects.
- Avoid assuming the technical knowledge of your readers.
- Avoid:
  - You can easily...
  - It's just...
  - Clearly...
  - And then simply...

- What to Expect
  - README
  - Code of Conduct
- Getting Started
  - Walk through the steps. No, _really_
  - Seek feedback
- Templates
  - PR Template
  - Tags, naming conventions, file structures.
  - Changelog

### Harvest Past Experience to be a Great Tech Writer - Mark F Iverson [ECA]
- the importance of considering the user's confusion - empathy
- being able to work with the SME to develop a relationship
- use a QA experience to explore the reason for using the documentation and the possibilities of breaking it
- project manage the docs

### How I Learned to Stop Worrying and Love the Command Line - Mike Jang
- configure an environment that'll be as close as possible to what a user would see

### Localize the Docs! - Paul Wallace
- not just translating, but fully designing to look, act, and feel local
- localization (**l10n**) service provider
    - guidance on best practices
    - modern tool systems
    - project management
    - vetted translators
- use vectorized graphics so that text and boundaries can be changeable and elastic

### In-app messaging (Contextual docs) - Meghan Mahar
- consider how that documentation is going to be included
    - will it be in the code? how it exists defines the tools you'll need / access you might have for changes
    - there are tools! google: "in-app messaging"
- keep it stylistically consistent with the rest of the platform
- consider your impact - think Clippy. you've potentially arrived uninvited.

## Tuesday
### Draw the Docs - Alicja Raszkowska
### Just Add Data: Make it easier to prioritize your documentation - Sarah Moir
- Prioritizing docs is hard
- Prioritize without data
  - Time
  - Asked for Last
  - PM says
  - Other
- Prioritize with data
  - Builds confidence in decision
  - Challenge or validate assumptions
  - improve customer understanding
- What kinds of data?
  - Long list, pull from slides.
  - Product usage data
    - Time in product, intra links clicked, data ingested, content created, etc
  - Site Metrics
    - Page views, session data, link clicks, referrers, button clicks, bounce rate, client UP, etc.
- Tie Questions to Data Types
  - What are people looking for and not finding
  - what do people want more help with?
  - What groups are we targeting?
- Find questions from data
  - Interest in new topics
  - new use cases
  - Site wide complexity or style issues
- How much data?
  - Not all of it, just what you can find to be relevant to the questions you want to answer.
  - Representative data
- How do you get that data?
  - Find what you can yourself
  - Talk to the departments that own the data - some data at some time, enough to prove the value needed to get more access in the future.
  - get whatever access you can.
- How do you analyze the data?
  - See what you have
  - use what you know
- Tools aren't magic
  - You will have to clean the data somehow.
- How to perform data analysis
  - top, rare, outlying values
  - Patterns and clusters across data
  - Split by different features
  - Split by time, product, releases, IP address, etc.
  - Combine data types (how many people using the product vs the docs)  -  Keep track of times and units to keep like with like
- Interpreting results
  - Add context from expertise
  - Pursue alternate explanations
  - Examine aggregate data - use caution, make sure you understand what the aggregate represents. A 3 star average rating, for example, can mean different things based on how the average is spread.
  - Draw realistic conclusions based on the available data
  - **Don't trust data blindly** - Examine outliers, who and what it represents. Analyze in context. Don't ignore what doesn't fit in your bias/expectations
- Example
  - Documentation for old API endpoints
  - Are people looking for these endpoints?
    - Forum data
    - search keyword data
    - Support case data
  - Look for different ways people are asking the question
  - Data analysis and interpretation
    - Add context based on experience
- Pull last slide to add

### Show Me the Money: How to Get Your Docs the Love and Support They Deserve - Matt Reiner [ECA]
- making the case for why it matters to the business
    - minimize duplicate content between support/sales/marketing///
    - reduce silos
    - better training
    - documentation is prevention-focused
    - **SWOT**: Strengths/Weaknesses/Opportunities/Threats
- business plan and goals
    - plan / goals / framework
    - elevator pitch
    - **USP**: Unique Selling Proposition
- Internal marketing
    - connect documentation with the brand
        - style, logos, language
    - marketing campaigns
- Support
    - in touch with users' feelings

### Show Me the Money: How to Get Your Docs the Love and Support They Deserve [AF]
- "We could do so much more if we just BLANK the docs"
  -  Business case / proposal
- Agenda
  - Intro, know the aud, learn the bus, connect with teams, bring it together
- Business Impact
  - Quote from Slide
- Duplicate efforts
  - Rewriting
  - Training
  - Silo reduction
- You
  - Prevention-focused vs Promotion-focused
  - Recognition = value - when we're not valued we exp self-pity and vulnerability
- Executive summary
  - What you're asking for and why
- SWOT Analysis
  - Strength, weakness, opportunities, threats
- Learn the business
  - Business plan and goals
    - The Plan
    - Goals
    - The Framework
- Elevator pitch
  - prepared speech
  - what and why
  - USP - Unique Selling Proposition -- What am I doing that no other team can do?
- Internal Marketing
  - Find moments "Seasons in which marketing happens"
  - Internal and external touchpoints
  - Brand connection
  - Show Me the Money: How to Get Your Docs the Love and Support They Deserve
- Business Description, marketing summary
  - Marketing
    - Campaigns
    - "How users talk"
    - Agreed terminology
  - Sales
    - Leads and opportunities
    - "What the product does"
    - Benefits
  - Support
    - Requests and SLAs
    - "User's feelings"
    - Feedback
- Competitor Analysis
  - Explore other teams and efforts within the company
- Operations overview
- Build the business case
  - Collect metrics
    - Data
    - Friends
    - Stories
  - Assemble your plan
    - Draft
    - Collaborate
    - Present internally before business leader
- Product Plan
  - Nitty Gritty of the effort
- Financial Plan
  - 

### SDK Reference Manuals: A flow-based approach - Chris Bush
- developer docs tend to
    - be an afterthought
    - not contain examples
    - incomplete
    - isolated from the overall docs experience
- put links in your SDK manual

### Documentation for Good: Knowledge as a tool for equity and inclusion - Riona MacNamara
- barriers that stop people from contributing to opensource
- psychological safety is paramount; documentation helps make it safe and equal
- documentation = knowledge is power. "documentation rebalances the power deficit"

### Product Documentation Strategy: What Is It and Why Do We Need It? - Kay Miles
- their process:
    - planning > content creation > signoff > publish > maintain
- "3 hats of doc strategy"
    1.  information architecture
    1.  content strategy
    1.  content engineering
- the issue of only fixing what we can see
    - user expectations VS not fixing the root cause
- users expect a consistent experience across products
- users expect a top result in google search
- if "content is king," then it's worth forming a content universe? around
- Books
    - _Designing Connected Content_ Atherton and Carrie Hanes
    - _Information Architecture_ Rosenfeld, Morville, Arango
    - _Intelligent Content, A Primer_ Rockley, Cooper, Abel
    - _Intertwingled_ Morville

## Notes / Asides
-
